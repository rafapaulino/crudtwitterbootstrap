;
(function (window, document, $, undefined) {
    'use strict';

    var app = (function () {

        var $private = {};
        var $public = {};

        $public.tooltips = function () {
            $('a').tooltip();
        }

        $public.popover = function() {
            $('.btn-popover').popover();
        }

        $public.formValidate = function () {
            if ($('form').length > 0) {
                //form validator 
                $("form").each(function(){
                    
                    $(this).validate();
                });
            }
        }

        $public.dateTimePicker = function () {
            if ($('.dateTimePicker').length > 0) {

                $('.dateTimePicker').datepicker({
                    language: 'pt-BR',
                    format: 'dd/mm/yyyy'
                });
            }
        }

        //funcoes para deletar os usuarios
        $public.deleteUsers = function () {
            //abre a modal
            $('.btn-delete').on('click',function(event) {
                event.preventDefault();

                if ($(this).attr('data-userid')) {
                    var $userSelected = $(this).data('userid');
                    $('#'+$userSelected).prop('checked',true).attr('checked',true);
                }
                //mostra a modal de deletar
                $('#delete-dialog').modal('show');
            });

            //retira a seleção quando a modal é fechada
            $('#cancelDeleteUsers').on('click',function(){
                $('.usersCheckbox').prop('checked',false).attr('checked',false);
            });

            //enviando os itens para deletar
            $('#deleteSelectedUsers').on('click',function(){
                var selecionados = $('.'+$('.checkall').val()+':checked').length;
                
                $('#delete-dialog').modal('hide');
                
                if (selecionados > 0) {
                    $('#formDel').submit();
                }
            });


            //selecionando todos os checkbox
            //funcao para selecionar todos os campos
            $('.checkall').on('click',function(){
              /*
               * pegando os campos que serao selecionados - eu indico isso atraves do atributo 
               * value do checkbox pai (que serve para selecionar).
               */
               var $this = $(this);
               var selecionar = $('.'+$this.val());
               //caso o checkbox esteja com o checked = true     
               if($this.is(':checked')){
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                        $(item).attr('checked',true);
                        $(item).prop('checked', true); //prop foi colocado por causa do twitter bootstrap
                   });
               //caso o checkbox nao esteja com o checked = true     
               }else{
                   //fazendo o loop pelos campos
                   $.each(selecionar,function(index,item){
                         $(item).attr('checked',false);
                         $(item).prop('checked', false); //prop foi colocado por causa do twitter bootstrap
                   });
               }
            
            });
        }

        return $public;
    })();

    // Global
    window.app = app;
    app.tooltips();
    app.popover();
    app.formValidate();
    app.deleteUsers();
    app.dateTimePicker();

})(window, document, jQuery);
