<?php
require_once 'includes/bootstrap.php';
$user->isLogged();

$id = intval($_GET['id']);

$form = array(
	'title' => 'Editar Usuário',
	'description' => 'Por favor edite as informações do usuário.',
	'action' => 'edit.php?id='.$id,
	'error' => '',
	'success' => '',
	'user' => $user->findOne($id)
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$message = $user->update($_POST,$id);

	if (is_array($message)) {

		$messages->setMessage('success',$message['success']);
		header("Location: list.php");
		exit;
	
	} else {
		$form['error'] = "Ocorreu um erro durante o processamento dos dados, por favor tente novamente!";
	}

}

echo $twig->render('form.html', $form);