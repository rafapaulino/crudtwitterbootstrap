### Rafael Paulino/Crud com o Twitter Bootstrap 3
===

Repositório:
https://bitbucket.org/rafapaulino/crudtwitterbootstrap

Usuário para acessar:
E-mail: rafaclasses@gmail.com
Senha: 123456

Modelagem do banco e sql de backup na pasta db do projeto.

Toda instalação dos componentes foi feita usando o bower, a compilação e união dos scripts e css foi feita utilizando o grunt.

Caso você deseje baixar do repositório esse será o passo a passo para instalar:

1 - Fazer o backup da base;
2 - Dar um composer install para add a Twig;
3 - Dar um bower install para add os componentes do front-end;
4 - Dar npm install para instalar o grunt e dependências;
5 - Comando grunt para executar as tarefas;

Com isso o sistema irá rodar na máquina sem problemas.
