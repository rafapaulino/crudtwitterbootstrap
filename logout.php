<?php
require_once 'includes/bootstrap.php';
session_destroy();

foreach($_COOKIE as $key=>$value) {
	unset($_COOKIE[$key]);
	setcookie($key,"",time()-3600,"/");
}

header("location:index.php");
exit();