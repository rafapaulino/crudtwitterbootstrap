<?php
require_once 'includes/bootstrap.php';
$user->isLogged();

$page = 1;
$perPage = 10;
$totalRecords = $user->getTotalRecords();

if (isset($_GET['page'])) {
	$page = intval($_GET['page']);
}
//cria a paginacao 
$paginator = new Pagination(
	$page, //pagina atual
	$perPage, //registros por pagina
	$totalRecords, //total de registros sem o limit do mysql
	10 //total de links de paginacao que serao exibidos na ul/li
);
$paginas = $paginator->getPages();

$data = array(
	'error' => $messages->getMessage('error'),
	'success' => $messages->getMessage('success'),
	'data' => $user->select($paginas['start'],$perPage),
	'page' => $page,
	'pagination' => $paginas
);
echo $twig->render('list.html', $data);