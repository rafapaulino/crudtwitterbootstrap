<?php

class FlashMessages 
{
	public function __construct()
	{
		if( !session_id() ) session_start();
	}

	public function setMessage($type,$message)
	{
		$_SESSION[$type] = $message;
	}

	public function getMessage($type)
	{
		if (isset($_SESSION[$type])) {
			$message = $_SESSION[$type];
			unset($_SESSION[$type]);
			return $message;
		} else {
			return '';
		}
	}
}