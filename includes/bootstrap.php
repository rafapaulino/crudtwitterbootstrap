<?php
if( !session_id() ) session_start();
header('Content-Type: text/html; charset=utf-8');
//arquivo que carrega as configurações iniciais para o funcionamento do programa

require_once dirname(__FILE__).'../../vendor/autoload.php';
require_once 'user.php';
require_once 'flashmessages.php';
require_once 'pagination.php';

//configuracoes da twig
define("TEMPLATE_PATH", dirname(__FILE__).'/../templates');
define("CACHE_PATH", dirname(__FILE__).'/../cache');

$loader = new Twig_Loader_Filesystem(TEMPLATE_PATH);
$twig = new Twig_Environment($loader, array(
    //'cache' => CACHE_PATH,
    'cache' => false
));

//configuracoes da pdo
$conn = new PDO(
    'mysql:host=localhost;dbname=tex', 'root', 'root',
    array(
        PDO::ATTR_PERSISTENT => true,
        PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
    )
);
$user = new User($conn);

//mensagens do sistema
$messages = new FlashMessages(); 