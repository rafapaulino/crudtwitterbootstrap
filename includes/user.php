<?php

class User 
{
	private $_conn;

	public function __construct($conn)
	{
		$this->_conn = $conn;
	}

	public function insert($formData)
	{
		
		//fazendo as conversoes necessarias antes de incluir no banco
		$formData['born'] = $this->convert_mysql_date($formData['born']);
		$formData['password'] = $this->crypt_password($formData['password']);

		$sql = "INSERT INTO users(
			username,
            password,
            hint,
            first_name,
            last_name,
            email,
            phone,
            website,
            settings,
            roles,
            born
            ) VALUES (
            :username, 
            :password, 
            :hint, 
            :first_name, 
            :last_name,
            :email,
            :phone,
            :website,
            :settings,
            :roles,
            :born
        )";                                
		$stmt = $this->_conn->prepare($sql);

		$stmt->bindParam(':username', $formData['username'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $formData['password'], PDO::PARAM_STR);
		$stmt->bindParam(':hint', $formData['hint'], PDO::PARAM_STR);
		$stmt->bindParam(':first_name', $formData['first_name'], PDO::PARAM_STR);
		$stmt->bindParam(':last_name', $formData['last_name'], PDO::PARAM_STR);
		$stmt->bindParam(':email', $formData['email'], PDO::PARAM_STR);
		$stmt->bindParam(':phone', $formData['phone'], PDO::PARAM_STR);
		$stmt->bindParam(':website', $formData['website'], PDO::PARAM_STR);
		$stmt->bindParam(':settings', $formData['settings'], PDO::PARAM_STR);
		$stmt->bindParam(':roles', $formData['roles'], PDO::PARAM_STR);
		$stmt->bindParam(':born', $formData['born'], PDO::PARAM_STR);
		$stmt->execute();
		$newId = $this->_conn->lastInsertId();

		return array(
			'id' => $newId,
			'success' => 'O usuário foi inserido com sucesso, id do usuário inserido é: '.$newId
		);
	}

	public function update($formData,$id)
	{
		//fazendo as conversoes necessarias antes de incluir no banco
		$formData['born'] = $this->convert_mysql_date($formData['born']);
		$formData['password'] = $this->crypt_password($formData['password']);

		$sql = "UPDATE users SET 
					username = :username, 
		            password = :password, 
		            hint = :hint,  
		            first_name = :first_name,  
		            last_name = :last_name,
		            email = :email, 
		            phone = :phone,  
		            website = :website,  
		            settings = :settings,
		            born = :born, 
		            roles = :roles    
		        WHERE id = :id";
		$stmt = $this->_conn->prepare($sql);

		$stmt->bindParam(':username', $formData['username'], PDO::PARAM_STR);
		$stmt->bindParam(':password', $formData['password'], PDO::PARAM_STR);
		$stmt->bindParam(':hint', $formData['hint'], PDO::PARAM_STR);
		$stmt->bindParam(':first_name', $formData['first_name'], PDO::PARAM_STR);
		$stmt->bindParam(':last_name', $formData['last_name'], PDO::PARAM_STR);
		$stmt->bindParam(':email', $formData['email'], PDO::PARAM_STR);
		$stmt->bindParam(':phone', $formData['phone'], PDO::PARAM_STR);
		$stmt->bindParam(':website', $formData['website'], PDO::PARAM_STR);
		$stmt->bindParam(':settings', $formData['settings'], PDO::PARAM_STR);
		$stmt->bindParam(':roles', $formData['roles'], PDO::PARAM_STR);
		$stmt->bindParam(':born', $formData['born'], PDO::PARAM_STR);
		$stmt->bindParam(':id', $id, PDO::PARAM_INT);

		$stmt->execute(); 

		return array(
			'id' => $id,
			'success' => 'O usuário foi editado com sucesso, id do usuário atualizado é: '.$id
		);      
	}

	public function delete($formData)
	{
		if (is_array($formData['user']) && count($formData['user']) > 0) {

			foreach ($formData['user'] as $user) {
				
				$sql = "DELETE FROM users WHERE id =  :id";
				$stmt = $this->_conn->prepare($sql);
				$stmt->bindParam(':id', $user, PDO::PARAM_INT);   
				$stmt->execute();
			}
		}

		return array(
			'success' => 'O(s) usuário(s) foram deletado(s) com sucesso!'
		);
	}

	public function select($offset,$recordsPage)
	{
		$consulta = $this->_conn->prepare("SELECT *, DATE_FORMAT(born,'%d/%m/%Y') AS niceDate FROM users ORDER BY username DESC LIMIT :recordsPage OFFSET :offset;");
		$consulta->bindParam(':recordsPage', $recordsPage, PDO::PARAM_INT);
		$consulta->bindParam(':offset', $offset, PDO::PARAM_INT);
		$consulta->execute();
		return $consulta->fetchAll(PDO::FETCH_ASSOC);
	}

	public function getTotalRecords()
	{
		$consulta = $this->_conn->query("SELECT * FROM users;");
		return $consulta->fetchColumn();
	}

	public function findOne($id)
	{
		$consulta = $this->_conn->prepare("SELECT *, DATE_FORMAT(born,'%d/%m/%Y') AS niceDate FROM users where id = :id;");
		$consulta->bindParam(':id', $id, PDO::PARAM_INT);
		$consulta->execute();
		return $consulta->fetch(PDO::FETCH_ASSOC);
	}

	public function login($formData)
	{
		$password = $this->crypt_password($formData['password']);
		$email = strip_tags($formData['email']);

		$consulta = $this->_conn->prepare("SELECT * FROM users where password = :password AND email = :email;");
		$consulta->bindParam(':password', $password, PDO::PARAM_STR);
		$consulta->bindParam(':email', $email, PDO::PARAM_STR);
		$consulta->execute();
		$user = $consulta->fetch(PDO::FETCH_ASSOC);
		$count = $consulta->rowCount();

		if ($count > 0) {
			
			$_SESSION['user'] = $user;

			return array(
				'success' => 'Seja Bem-Vindo(a) '.$user['first_name']
			);
		
		} else {
			
			return array(
				'error' => 'Usuário não encontrado no sistema!'
			);
		}
	}


	public function isLogged()
	{
		if (isset($_SESSION['user'])) {

			$user = $this->findOne($_SESSION['user']['id']);
			
			if (count($user) == 0) {
				header("Location: logout.php");
				exit;
			}

		} else {
			header("Location: logout.php");
			exit;
		}
	}


	private function convert_mysql_date($date)
	{
		$oDate = DateTime::createFromFormat('d/m/Y',$date);
		
		if (!is_object($oDate)) {
			$oDate = new DateTime($date);
		} 
		return $oDate->format("Y-m-d");
	}

	private function crypt_password($password)
	{
		$hash = "";
		foreach (hash_algos() as $v) {
        	$hash.= hash($v, $password, false);
        }
        return sha1(md5($hash));
	}
}