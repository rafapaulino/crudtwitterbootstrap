<?php

class Pagination
{
	protected $_page;
	protected $_recordsPerPage;
	protected $_start;
	protected $_totalRecords;
	protected $_listOfPages = array();
	protected $_totalListOfPagesInArray;
	protected $_totalPages = 0;
	protected $_nextPage = 1;
	protected $_previousPage = 1;
	protected $_firstPage = 1;
	protected $_lastPage = 1;


	public function __construct($page,$recordsPerPage,$totalRecords,$totalListOfPagesInArray)
	{
		$this->_page = $page;
		$this->_recordsPerPage = $recordsPerPage;
		$this->_totalRecords = $totalRecords;
		$this->_totalListOfPagesInArray = $totalListOfPagesInArray;
		$this->_start =  ($this->_page - 1) * $this->_recordsPerPage;
	}

	public function getPages()
	{
		$this->_totalPages = intval(ceil($this->_totalRecords/$this->_recordsPerPage));
		$this->_lastPage = $this->_totalPages;

		$this->setNextPage();
		$this->setPreviousPage();
		$this->setListOfPages();
		

		return array (
			'page' => $this->_page,
			'recordsPerPage' => $this->_recordsPerPage,
			'start' => $this->_start,
			'totalRecords' => $this->_totalRecords,
			'listOfPages' => $this->_listOfPages,
			'totalListOfPagesInArray' => $this->_totalListOfPagesInArray,
			'totalPages' => $this->_totalPages,
			'nextPage' => $this->_nextPage,
			'previousPage' => $this->_previousPage,
			'firstPage' => $this->_firstPage,
			'lastPage' => $this->_lastPage
		);		
	}

	private function setNextPage()
	{
		$nextPage = $this->_page + 1;
		
		if ($nextPage >= $this->_totalPages)
		$nextPage = $this->_totalPages;
				   
		$this->_nextPage = intval($nextPage);
	}

	private function setPreviousPage()
	{
	    $previousPage = $this->_page - 1;
	   
	   	if($previousPage <= 1)
	   	$previousPage = 1;
		$this->_previousPage = intval($previousPage);
	}

	private function setListOfPages()
	{
		$this->_listOfPages = range(1, $this->_totalPages);

		if($this->_totalPages > $this->_totalListOfPagesInArray) {

			$currentIndex = $this->_page - 1;
            $pause = ($this->_totalPages - $this->_totalListOfPagesInArray);

            if($this->_page > $pause)
            $currentIndex = $pause;

        	$this->_listOfPages = array_slice($this->_listOfPages,$currentIndex,$this->_totalListOfPagesInArray);
		}
	}

}