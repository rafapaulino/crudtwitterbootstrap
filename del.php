<?php
require_once 'includes/bootstrap.php';
$user->isLogged();

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$message = $user->delete($_POST);

	if (is_array($message)) {
		$messages->setMessage('success',$message['success']);
	} else {
		$messages->setMessage('error','Ocorreu um erro durante o processamento dos dados, por favor tente novamente!');
	}

} else {
	$messages->setMessage('error','A ação solicitada não pode ser executada!');
}

header("Location: list.php");