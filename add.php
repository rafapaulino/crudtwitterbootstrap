<?php
require_once 'includes/bootstrap.php';
$user->isLogged();

$form = array(
	'title' => 'Adicionar Usuário',
	'description' => 'Por favor adicione as informações do usuário.',
	'action' => 'add.php',
	'error' => '',
	'success' => ''
);

if ($_SERVER["REQUEST_METHOD"] == "POST") {

	$message = $user->insert($_POST);

	if (is_array($message)) {
		
		$messages->setMessage('success',$message['success']);
		header("Location: list.php");
		exit;

	} else {
		$form['error'] = "Ocorreu um erro durante o processamento dos dados, por favor tente novamente!";
	}
}

echo $twig->render('form.html', $form);