<?php
require_once 'includes/bootstrap.php';
$data = array();

if ($_SERVER["REQUEST_METHOD"] == "POST" && count($_POST) > 0 && isset($_POST['password']) && isset($_POST['email']) ) {

	$message = $user->login($_POST);

	if (is_array($message)) {

		if (isset($message['success'])) {

			$messages->setMessage('success',$message['success']);
			header("Location: list.php");
			exit;
		
		} else {
			$data['error'] = $message['error'];
		}
	
	} else {
		$data['error'] = "Ocorreu um erro durante o login, por favor tente novamente!";
	}

}

echo $twig->render('index.html', $data);