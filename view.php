<?php
require_once 'includes/bootstrap.php';
$user->isLogged();

if (isset($_GET['id'])) {
	
	$id = intval($_GET['id']);

	$form = array(
		'title' => 'Ver Usuário',
		'description' => 'Nessa tela você vê as informações do usuário.',
		'user' => $user->findOne($id)
	);
	
	echo $twig->render('view.html', $form);

} else {
	$messages->setMessage('error',"Usuário não enconrado!");
	header("Location: list.php");
}
