//variaveis globais de configuracao
var config = {
  assets: 'assets',
  css_app: 'assets/css',
  css_sass: 'assets/scss',
  js_app: 'assets/js/app',
  js_dev: 'assets/js/dev'
}


module.exports = function(grunt) {
    'use strict';
 
    // configuração do projeto
    var gruntConfig = {
        config: config,
        pkg: grunt.file.readJSON('package.json'),
        bower: grunt.file.readJSON('./.bowerrc'),

        copy: {
            target: {
                files: [{
                  expand: true,
                  cwd: '<%= bower.directory %>/bootstrap',
                  src: 'fonts/*',
                  dest: '<%= config.assets %>'
                },
                {
                  expand: true,
                  cwd: '<%= bower.directory %>/fontawesome',
                  src: 'fonts/*',
                  dest: '<%= config.assets %>'
                }]
            }
        },

        uglify: {
            target: {
                files: {
                    '<%= config.js_app %>/app.min.js': [
                        '<%= bower.directory %>/jquery/dist/jquery.min.js',
                        '<%= bower.directory %>/bootstrap/dist/js/bootstrap.min.js',
                        '<%= bower.directory %>/bootstrap-datepicker/js/bootstrap-datepicker.js',
                        '<%= bower.directory %>/bootstrap-datepicker/js/locales/bootstrap-datepicker.pt-BR.js',
                        '<%= bower.directory %>/jquery-validation/dist/jquery.validate.min.js',
                        '<%= bower.directory %>/jquery-validation/dist/additional-methods.min.js',
                        '<%= bower.directory %>/jquery-validation/src/localization/messages_pt_BR.js',
                        '<%= config.js_dev %>/app.js'                        
                    ]
                }
            }
        },

        sass: {
            dist: {
                options: {
                    compass: true,
                    style: 'compressed'
                },
                files: {                        
                    '<%= config.css_app %>/app.css': '<%= config.css_sass %>/main.scss'
                }
            }
        },

        cssmin: {
            target: {
                files: {
                    '<%= config.css_app %>/app.min.css': [
                        '<%= bower.directory %>/fontawesome/css/font-awesome.min.css',
                        '<%= bower.directory %>/bootstrap/dist/css/bootstrap.min.css',
                        '<%= bower.directory %>/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css',
                        '<%= config.css_app %>/app.css'
                    ]
                }
            }
        }

    };
 
    grunt.initConfig(gruntConfig);

    // plugins
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    //grunt.loadNpmTasks('grunt-contrib-watch');
    //grunt.loadNpmTasks('grunt-cache-breaker');

    // tarefas
    grunt.registerTask('default', ['copy', 'uglify', 'sass', 'cssmin']);
    //grunt.registerTask('deploy', ['cachebreaker']);
    
    // Tarefa para Watch
    //grunt.registerTask( 'w', [ 'watch' ] );
};